# shellcheck disable=SC2155
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo docker -d > /dev/null 2>&1 &

export COMMIT_TIME=$(git show -s --format=%ci $CI_COMMIT_SHA)
export DATE_FORMAT=${COMMIT_TIME:2:8}
export COMMIT_MESSAGE=$(git log --format=%B -n 1 $CI_COMMIT_SHA)
export LOWERCASE_COMMIT_MESSAGE=${COMMIT_MESSAGE,,}
export FIRST_EIGHT_CHARS=${LOWERCASE_COMMIT_MESSAGE:0:8}
export COMMIT_MESSAGE_FORMAT="${FIRST_EIGHT_CHARS// /-}"
export VERSION=$DATE_FORMAT-$COMMIT_MESSAGE_FORMAT

docker build -t $VERSION .
docker tag $VERSION  aboghazal96/docker-practice:$VERSION
docker login -u aboghazal96 -p Ghazal!?@123  https://index.docker.io/v1/
docker push aboghazal96/docker-practice:$VERSION
